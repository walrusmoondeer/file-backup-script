#!/usr/bin/env bash 

#TODO: if file doesnt permit movement
#TODO: verbose processes

# Colors for colorful prompts/text:
RED='\033[0;31m'
NC='\033[0m' # No Color

read -rp "Script only works on archlinux, do you want to continue [Y/n]? : " terminate
if [[ "${terminate}" =~ [Nn] ]]; then 
  echo "You have chosen to terminate the script"; exit 
fi 

read -rp "Do you want to compress your backup [y/N]? : " compress

if [ -r "$PWD"/backup_paths ]; then
  # Read lines from backup_paths into the 'lines' array using mapfile
  mapfile -t lines < backup_paths

  # Declare an empty array to store the filtered lines
  home_contents=()

  # Iterate over the 'lines' array and exclude elements starting with '#'
  for line in "${lines[@]}"; do
      if [[ ! $line =~ ^# ]]; then
          home_contents+=("${line// /}") # the three slashes remove redundant spaces
      fi
  done
else 
  echo -e "${RED}This script requires backup_paths file to be present in the working directory!${NC}"; exit
fi

# if backup drive path is not added when executing, 
# /* then ask for backup drive path to be entered manually:
if [ -z "$1" ]; then 
  read -rp "Please enter the full path to backup drive: " backup_path
else 
  backup_path="$1"
fi

# Test if Backup drive even exists
if [ -d "${backup_path}" ]; then
  echo "All is good, backup drive has been found"
else
  echo -e "${RED}Drive was not found; Command will stop; Try again${NC}"; exit
fi 

# Making Backup directory
# Format --> year-month-day_device-backup
backup_dir_name="$(date "+%Y-%m-%d")_$(uname -n)-backup"
mkdir -p "${backup_path}"/System-backups/"${backup_dir_name}"
backup_dir="${backup_path}/System-backups/${backup_dir_name}"

# Backing-up specified content from '$PWD/backup_paths' 
for i in "${home_contents[@]}"; do 
  if [ -e "$HOME"/"$i" ]; then
    cp -r "$HOME"/"$i" "${backup_dir}"/
  else 
    echo -e "${RED}File doesnt exist: ${HOME}/${i}${NC}"
  fi
done 

cd "${backup_path}"/System-backups || exit;

if [[ "${compress}" =~ [Yy] ]]; then 
  echo "Using gzip compression algorith"
  tar cvzf "${backup_dir_name}".tar "${backup_dir}" && rm -rf "${backup_dir}"
else
  tar cvf "${backup_dir_name}".tar "${backup_dir}" && rm -rf "${backup_dir}"
fi

echo "To Un-Archive the backup do: tar xvf <archived_file_path> , NOTE: add a 'z' flag if archived file uses gzip compression"
