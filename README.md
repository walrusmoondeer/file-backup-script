**Bash script I use to backup my files faster and without forgetting anything important.**
**NOTE:** this script only works on archlinux, for now at least.
- **backup_paths** <-- Should contain paths to files/directories in your home directory for backup, remove/add paths as you desire, every line that begins with a hash ('#') is not read as a path. 
- **auto_backup_script.bash** <-- Automatically backups files specified in **backup_paths**, the script offers file compression as an option. The script creates a System-backup directory in specified backup drive, and creates a backup file with current date. To specify what you want backed up, edit 'backup_paths'. **NOTE:** 'backup_paths' should be in your working directory when you run 'auto_backup_script.bash'
